﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApplication1
{
    public partial class levels : Window
    {
        public levels()
        {
            InitializeComponent();
        }
        private void select_level_1(object sender, RoutedEventArgs e)
        {
            MainWindow main = ((MainWindow)Owner);
            main.levelName = int.Parse(((TextBlock)((Button)sender).Resources["Level"]).Text);
            main.InitializeComponent2();
            GetWindow(this).Close();
        }
        private void cancel(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            Window1 w1 = new Window1();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }
    }
}
