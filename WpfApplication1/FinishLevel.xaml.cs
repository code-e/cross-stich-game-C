﻿using System.Windows;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для FinishLevel.xaml
    /// </summary>
    public partial class FinishLevel : Window
    {
        public int warnings;
        public int cross;
        public string time;

        public FinishLevel()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Warnings.Content = warnings.ToString();
            Cross.Content = cross.ToString();
            Time.Content = time;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            Window3 w3 = new Window3();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            MainWindow main = ((MainWindow)Owner);
            main.Close();
        }
    }
}
