﻿using System.Windows;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        public Window3()
        {
            InitializeComponent();
        }

        private void select_1_level(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Owner).levelHigt = 1;
            GetWindow(this).Close();
            levels w1 = new levels();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }

        private void select_2_level(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Owner).levelHigt = 2;
            GetWindow(this).Close();
            levels2 w1 = new levels2();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }

        private void select_3_level(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Owner).levelHigt = 3;
            GetWindow(this).Close();
            levels3 w1 = new levels3();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            Window1 w1 = new Window1();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }
    }
}
