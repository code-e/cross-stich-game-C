﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Exit : Window
    {
        private Canvas canvas;
        public Exit()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            MainWindow main = ((MainWindow)Owner);
            main.Close();
        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            MainWindow main = ((MainWindow)Owner);
            main.Close();
        }
        private void button6_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Opacity = 0;
            About w3 = new About();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
            GetWindow(this).Opacity = 1;
        }
    }
}
