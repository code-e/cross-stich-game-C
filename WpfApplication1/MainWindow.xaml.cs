﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Resources;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Threading;
using System.ComponentModel;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String currentColor = "#000000";
        int currentColorIndex = 0;
        Border border = new Border();
        Image img = new Image();
        TextBlock textBlock = new TextBlock();
        WrapPanel wrapPanel = new WrapPanel();
        Label labelColor = new Label();
        public int levelHigt = 0;
        public int levelName = 0;
        public int allLevelCrossCount = 0;
        public int currentLevelCrossCount = 0;
        int warningsCount = 0;
        int allWarningsCount = 0;
        Boolean isRaspar = false;
        Boolean notActivated = true;
        Boolean soundPlay = true;
        DrawingGroup group = new DrawingGroup();

        public MainWindow()
        {
            SplashScreen splashScreen = new SplashScreen("images/cat.png");
            splashScreen.Show(true);
            InitializeComponent();
        }

        private void TheBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                return;
            }

            Border border = (Border)sender;
            if (int.Parse(border.Resources["status"].ToString()) == 1)
            {
                return;
            }

            if (isRaspar)
            {
                textBlock = new TextBlock();
                textBlock.Text = border.Resources["indexColor"].ToString();
                textBlock.FontSize = 18;
                textBlock.VerticalAlignment = VerticalAlignment.Center;
                textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                Sound.play("./sound/02.mp3", soundPlay);
                border.Child = textBlock;
                if (int.Parse(border.Resources["status"].ToString()) == -1)
                {
                    warningsCount--;
                }
                border.Resources["status"] = 0;
                Warnings.Text = "Ошибок: " + warningsCount.ToString();
                return;
            }


            int colorIndex = int.Parse(border.Resources["indexColor"].ToString());
            BitmapDecoder dec = BitmapDecoder.Create(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/cross.png"), BitmapCreateOptions.None, BitmapCacheOption.Default);
            BitmapFrame image = dec.Frames[0];
            byte[] pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
            image.CopyPixels(pixels, image.PixelWidth * 4, 0);
            Color color = Color.FromRgb(255, 255, 255);
            try
            {
                color = (Color)ColorConverter.ConvertFromString(currentColor);
            }
            catch
            {
                //MessageBox.Show("Err");
            }

            for (int i = 0; i < pixels.Length / 4; ++i)
            {
                byte b = pixels[i * 4];
                byte g = pixels[i * 4 + 1];
                byte r = pixels[i * 4 + 2];
                byte a = pixels[i * 4 + 3];

                if (a != 0 && r > 120 && g > 120 && b > 120)
                {
                    pixels[i * 4 + 2] = color.R;
                    pixels[i * 4 + 1] = color.G;
                    pixels[i * 4] = color.B;
                }
                else
                {
                    Color newcolor = color;
                    pixels[i * 4 + 2] = (byte)(newcolor.R / 1.7);
                    pixels[i * 4 + 1] = (byte)(newcolor.G / 1.7);
                    pixels[i * 4] = (byte)(newcolor.B / 1.7);
                }
            }

            WriteableBitmap bitmap = new WriteableBitmap(image.PixelWidth, image.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
            bitmap.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
            Sound.play("./sound/01.mp3", soundPlay);
            var group = new DrawingGroup();
            group.Children.Add(new ImageDrawing(bitmap, new Rect(0, 0, 50, 50)));
            group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/over_cross.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));
            if (int.Parse(border.Resources["status"].ToString()) == 0)
            {
                border.Resources["status"] = 1;
            }
            if (int.Parse(border.Resources["status"].ToString()) == -1)
            {
                border.Resources["status"] = 1;
                warningsCount--;
                Warnings.Text = "Ошибок: " + warningsCount.ToString();
            }
            if (currentColorIndex != colorIndex)
            {
                group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/warning.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));
                border.Resources["status"] = -1;
                warningsCount++;
                allWarningsCount++;
                Warnings.Text = "Ошибок: " + warningsCount.ToString();
            }
            else
            {
                currentLevelCrossCount++;
                double raz = allLevelCrossCount - currentLevelCrossCount;
                double r2 = raz / allLevelCrossCount;
                int r = (int)(100.0 - (r2 * 100.0));
                Progress.Text = "Выполнено: " + r.ToString() + "%";
            }

            border.Background = new SolidColorBrush(Colors.White);
            Image img = new Image
            {
                Source = new DrawingImage(group)
            };
            border.Child = img;

            if (currentLevelCrossCount == allLevelCrossCount)
            {
                FinishLevel fl = new FinishLevel();
                fl.Owner = this;
                fl.warnings = allWarningsCount;
                fl.time = Timer.Text;
                fl.cross = allLevelCrossCount;
                fl.ShowDialog();
            }
        }

        private int seconds { get; set; }

        public async void InitializeComponent2()
        {
            Window2 w2 = new Window2();
            w2.Owner = this;
            w2.Width = this.ActualWidth;
            w2.Height = this.ActualHeight;

            w2.WindowStyle = WindowStyle.None;
            w2.WindowState = WindowState.Maximized;
            w2.ResizeMode = ResizeMode.NoResize;
            w2.Topmost = true;

            canvas.Children.Clear();
            wrap.Children.Clear();
            allLevelCrossCount = 0;
            currentLevelCrossCount = 0;
            currentColorIndex = 0;
            warningsCount = 0;

            await Task.Run(() =>
            {
                Application.Current.Dispatcher.Invoke(() => w2.Show());
                readScheme();
                Application.Current.Dispatcher.Invoke(() => { w2.Close(); });
            });

            seconds = 0;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (pause) return;
            TimeSpan ts = TimeSpan.FromSeconds(seconds++);
            var currentValue = ts;
            Timer.Text = (currentValue.Hours < 10 ? "0" : "") + currentValue.Hours.ToString() + ':' +
                (currentValue.Minutes < 10 ? "0" : "") + currentValue.Minutes.ToString() + ':' +
                (currentValue.Seconds < 10 ? "0" : "") + currentValue.Seconds.ToString();
        }

        private void readScheme()
        {
            StreamResourceInfo imageInfo = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//levels/" + levelHigt.ToString() + "/" + levelName.ToString() + ".json"));
            StreamReader sr = new StreamReader(imageInfo.Stream);
            var content = sr.ReadToEnd();

            dynamic magic = JsonConvert.DeserializeObject(content);

            currentColor = magic["colors"][0]["rgb"];
            currentColorIndex = 0;

            int j = 0;
            foreach (var item in magic["colors"])
            {
                wrapPanel.Dispatcher.Invoke((Action)(() =>
                {
                    Color color;
                    string s = magic["colors"][j]["rgb"];
                    try
                    {
                        color = (Color)ColorConverter.ConvertFromString(s);
                    }
                    catch
                    {
                        color = (Color)ColorConverter.ConvertFromString("#000000");
                    }

                    wrapPanel = new WrapPanel();
                    labelColor = new Label();
                    labelColor.Content = j.ToString();
                    labelColor.Foreground = Brushes.White;
                    labelColor.FontSize = 20;
                    labelColor.Width = 50;
                    labelColor.HorizontalContentAlignment = HorizontalAlignment.Right;

                    BitmapDecoder dec = BitmapDecoder.Create(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/cross.png"), BitmapCreateOptions.None, BitmapCacheOption.Default);
                    BitmapFrame image = dec.Frames[0];
                    byte[] pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
                    image.CopyPixels(pixels, image.PixelWidth * 4, 0);

                    for (int i = 0; i < pixels.Length / 4; ++i)
                    {
                        byte b = pixels[i * 4];
                        byte g = pixels[i * 4 + 1];
                        byte r = pixels[i * 4 + 2];
                        byte a = pixels[i * 4 + 3];

                        if (a != 0)
                        {
                            pixels[i * 4 + 2] = color.R;
                            pixels[i * 4 + 1] = color.G;
                            pixels[i * 4] = color.B;
                        }
                    }

                    WriteableBitmap bitmap = new WriteableBitmap(image.PixelWidth, image.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
                    bitmap.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
                    img = new Image();
                    img.Source = bitmap;
                    img.Width = 30;
                    img.Height = 30;
                    img.HorizontalAlignment = HorizontalAlignment.Right;
                    wrapPanel.Children.Add(labelColor);
                    wrapPanel.Children.Add(img);
                    wrapPanel.MouseLeftButtonDown += WrapPanel_MouseLeftButtonDown;
                    wrap.Children.Add(wrapPanel);
                }));
                j++;
            }


            int I = 0, J = 0;

            foreach (var row in magic["data"])
            {
                J = 0;
                foreach (var col in row)
                {
                    allLevelCrossCount++;
                    string ss = "";
                    try
                    {
                        ss = magic["colors"][(int)col]["rgb"];
                    }
                    catch (Exception e)
                    {
                        ss = magic["colors"][0]["rgb"];
                    }
                    Color color = (Color)ColorConverter.ConvertFromString(ss);

                    border.Dispatcher.Invoke((Action)(() =>
                    {
                        textBlock = new TextBlock();
                        textBlock.Text = (string)col;
                        textBlock.FontSize = 18;
                        textBlock.VerticalAlignment = VerticalAlignment.Center;
                        textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                        border = new Border();
                        border.Width = 50;
                        border.Height = 50;
                        border.BorderBrush = new SolidColorBrush(Colors.Gray);
                        border.Background = new SolidColorBrush(Colors.AliceBlue);
                        border.BorderThickness = new Thickness(1);
                        border.MouseDown += TheBorder_MouseDown;
                        border.Child = textBlock;
                        border.ToolTip = (string)col;
                        border.Resources["indexColor"] = col;
                        border.Resources["status"] = 0;
                        Canvas.SetLeft(border, J * 50);
                        Canvas.SetTop(border, I * 50);
                        canvas.Children.Add(border);
                    }));

                    J++;
                }
                I++;
            }
            canvas.Dispatcher.Invoke((Action)(() =>
            {
                canvas.Width = J * 50;
                canvas.Height = I * 50;
                scrollViewer.UpdateLayout();
            }));
        }

        private void WrapPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WrapPanel wrapPanel = (WrapPanel)sender;
            IEnumerable<Label> l = wrapPanel.Children.OfType<Label>();
            int colorIndex = int.Parse(l.ToArray<Label>()[0].Content.ToString());
            StreamResourceInfo imageInfo = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//levels/" + levelHigt.ToString() + "/" + levelName.ToString() + ".json"));
            StreamReader sr = new StreamReader(imageInfo.Stream);
            var content = sr.ReadToEnd();
            dynamic magic = JsonConvert.DeserializeObject(content);
            currentColor = magic["colors"][colorIndex]["rgb"];
            currentColorIndex = colorIndex;

            var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/igla.cur")).Stream;
            canvas.Cursor = new Cursor(myCur);
            isRaspar = false;

            foreach (Border b in canvas.Children)
            {
                if (int.Parse(b.Resources["indexColor"].ToString()) == colorIndex &&
                    int.Parse(b.Resources["status"].ToString()) == 0)
                {
                    b.Background = new SolidColorBrush(Colors.RosyBrown);
                    scrollViewer.ScrollToVerticalOffset(Canvas.GetTop(b) - this.Width / 2);
                    scrollViewer.ScrollToHorizontalOffset(Canvas.GetLeft(b) - this.Height / 2);
                }
                else
                {
                    b.Background = new SolidColorBrush(Colors.AliceBlue);
                }
            }
        }

        private bool isMoving = false;                 
        private bool isDeferredMovingStarted = false;
        private Point? startPosition = null;
        private double slowdown = 10;                 
        private void ScrollViewer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.isMoving == true)
                this.CancelScrolling();
            else if (e.ChangedButton == MouseButton.Right && e.ButtonState == MouseButtonState.Pressed)
            {
                if (this.isMoving == false)
                {
                    this.isMoving = true;
                    this.startPosition = e.GetPosition(sender as IInputElement);
                    this.isDeferredMovingStarted = true;
                }
            }
        }

        private void ScrollViewer_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right && e.ButtonState == MouseButtonState.Released && this.isDeferredMovingStarted != true)
                this.CancelScrolling();
        }
        private void CancelScrolling()
        {
            this.isMoving = false;
            this.startPosition = null;
            this.isDeferredMovingStarted = false;
        }

        private void ScrollViewer_MouseMove(object sender, MouseEventArgs e)
        {
            var sv = sender as ScrollViewer;

            if (this.isMoving && sv != null)
            {
                this.isDeferredMovingStarted = false;
                var currentPosition = e.GetPosition(sv);
                var offset = currentPosition - startPosition.Value;
                offset.Y /= slowdown;
                offset.X /= slowdown;
                sv.ScrollToVerticalOffset(sv.VerticalOffset + offset.Y);
                sv.ScrollToHorizontalOffset(sv.HorizontalOffset + offset.X);
            }
        }

        private void button_x1_Click(object sender, RoutedEventArgs e)
        {
            ScaleSlider.Value = 0.1;
        }
        private void button_x5_Click(object sender, RoutedEventArgs e)
        {
            ScaleSlider.Value = 0.5;
        }
        private void button_x10_Click(object sender, RoutedEventArgs e)
        {
            ScaleSlider.Value = 1;
        }
        private void button_game(object sender, RoutedEventArgs e)
        {
            Window1 gameMenu = new Window1(canvas);
            gameMenu.Owner = this;
            gameMenu.ShowDialog();
        }
        private void search_warning(object sender, RoutedEventArgs e)
        {
            foreach (Border b in canvas.Children)
            {
                if (int.Parse(b.Resources["status"].ToString()) == -1)
                {
                    scrollViewer.ScrollToVerticalOffset(Canvas.GetTop(b));
                    scrollViewer.ScrollToHorizontalOffset(Canvas.GetLeft(b));
                    break;
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/raspar_cur.cur")).Stream;
            canvas.Cursor = new Cursor(myCur);
            isRaspar = true;
        }

        private void MenuItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            test();
        }

        private async void test()
        {
            BitmapFrame image;
            BitmapDecoder dec = BitmapDecoder.Create(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/cross.png"), BitmapCreateOptions.None, BitmapCacheOption.Default);
            StreamResourceInfo imageInfo = Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//levels/" + levelHigt.ToString() + "/" + levelName.ToString() + ".json"));
            StreamReader sr = new StreamReader(imageInfo.Stream);
            var content = sr.ReadToEnd();
            dynamic magic = JsonConvert.DeserializeObject(content);
            int lastColorIndex = -1;
            Image lastImg = new Image();

            foreach (Border border in canvas.Children)
            {
                await Task.Run(() =>
                {
                    border.Dispatcher.Invoke((Action)(() =>
                {
                    if (int.Parse(border.Resources["status"].ToString()) == 1 || int.Parse(border.Resources["status"].ToString()) == -1)
                    {
                        return;
                    }

                    int colorIndex = int.Parse(border.Resources["indexColor"].ToString());
                    if (lastColorIndex == colorIndex)
                    {
                        border.Background = new SolidColorBrush(Colors.White);
                        img = new Image
                        {
                            Source = new DrawingImage(group)
                        };
                        border.Resources["status"] = 1;
                        border.Child = img;
                        currentLevelCrossCount++;
                        return;
                    }


                    image = dec.Frames[0];
                    byte[] pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
                    image.CopyPixels(pixels, image.PixelWidth * 4, 0);

                    currentColor = magic["colors"][colorIndex]["rgb"];
                    currentColorIndex = colorIndex;

                    Color color = Color.FromRgb(255, 255, 255);
                    try
                    {
                        color = (Color)ColorConverter.ConvertFromString(currentColor);
                    }
                    catch
                    {
                        //MessageBox.Show("Err");
                    }

                    for (int i = 0; i < pixels.Length / 4; ++i)
                    {
                        byte b = pixels[i * 4];
                        byte g = pixels[i * 4 + 1];
                        byte r = pixels[i * 4 + 2];
                        byte a = pixels[i * 4 + 3];

                        if (a != 0 && r > 120 && g > 120 && b > 120)
                        {
                            pixels[i * 4 + 2] = color.R;
                            pixels[i * 4 + 1] = color.G;
                            pixels[i * 4] = color.B;
                        }
                        else
                        {
                            Color newcolor = color;
                            pixels[i * 4 + 2] = (byte)(newcolor.R / 1.7);
                            pixels[i * 4 + 1] = (byte)(newcolor.G / 1.7);
                            pixels[i * 4] = (byte)(newcolor.B / 1.7);
                        }
                    }
                    WriteableBitmap bitmap = new WriteableBitmap(image.PixelWidth, image.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
                    bitmap.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
                    group = new DrawingGroup();
                    group.Children.Add(new ImageDrawing(bitmap, new Rect(0, 0, 50, 50)));
                    group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/over_cross.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));

                    if (currentColorIndex != colorIndex)
                    {
                        group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/warning.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));
                        border.Resources["status"] = -1;
                        warningsCount++;
                        Warnings.Text = "Ошибок: " + warningsCount.ToString();
                    }
                    else
                    {
                        border.Resources["status"] = 1;
                        currentLevelCrossCount++;
                        double raz = allLevelCrossCount - currentLevelCrossCount;
                        double r2 = raz / allLevelCrossCount;
                        int r = (int)(100.0 - (r2 * 100.0));
                        Progress.Text = "Выполнено: " + r.ToString() + "%";
                    }

                    border.Background = new SolidColorBrush(Colors.White);
                    img = new Image
                    {
                        Source = new DrawingImage(group)
                    };
                    border.Child = img;

                    lastImg = img;
                    lastColorIndex = colorIndex;
                }));
                });
                if (currentLevelCrossCount == allLevelCrossCount - 4)
                {
                    break;
                }
            }
        }

        private async void loadedRender()
        {
            BitmapFrame image;
            BitmapDecoder dec = BitmapDecoder.Create(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/cross.png"), BitmapCreateOptions.None, BitmapCacheOption.Default);
            StreamResourceInfo imageInfo = Application.GetResourceStream(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//levels/" + levelHigt.ToString() + "/" + levelName.ToString() + ".json"));
            StreamReader sr = new StreamReader(imageInfo.Stream);
            var content = sr.ReadToEnd();
            dynamic magic = JsonConvert.DeserializeObject(content);
            
            foreach (Border border in canvas.Children)
            {
                await Task.Run(() =>
                {
                    border.Dispatcher.Invoke((Action)(() =>
                    {
                        int colorIndex = int.Parse(border.Resources["indexColor"].ToString());

                        if (int.Parse(border.Resources["status"].ToString()) == 1 || int.Parse(border.Resources["status"].ToString()) == -1)
                        {
                            image = dec.Frames[0];
                            byte[] pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
                            image.CopyPixels(pixels, image.PixelWidth * 4, 0);

                            currentColor = magic["colors"][colorIndex]["rgb"];
                            currentColorIndex = colorIndex;

                            Color color = Color.FromRgb(255, 255, 255);
                            try
                            {
                                color = (Color)ColorConverter.ConvertFromString(currentColor);
                            }
                            catch
                            {

                            }

                            for (int i = 0; i < pixels.Length / 4; ++i)
                            {
                                byte b = pixels[i * 4];
                                byte g = pixels[i * 4 + 1];
                                byte r = pixels[i * 4 + 2];
                                byte a = pixels[i * 4 + 3];

                                if (a != 0 && r > 120 && g > 120 && b > 120)
                                {
                                    pixels[i * 4 + 2] = color.R;
                                    pixels[i * 4 + 1] = color.G;
                                    pixels[i * 4] = color.B;
                                }
                                else
                                {
                                    Color newcolor = color;
                                    pixels[i * 4 + 2] = (byte)(newcolor.R / 1.7);
                                    pixels[i * 4 + 1] = (byte)(newcolor.G / 1.7);
                                    pixels[i * 4] = (byte)(newcolor.B / 1.7);
                                }
                            }

                            group = new DrawingGroup();

                            if (int.Parse(border.Resources["status"].ToString()) == -1)
                            {
                                group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/warning.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));
                                Warnings.Text = "Ошибок: " + warningsCount.ToString();
                            } else {
                                WriteableBitmap bitmap = new WriteableBitmap(image.PixelWidth, image.PixelHeight, 96, 96, PixelFormats.Bgra32, null);
                                bitmap.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
                                group.Children.Add(new ImageDrawing(bitmap, new Rect(0, 0, 50, 50)));
                                group.Children.Add(new ImageDrawing(new BitmapImage(new Uri("pack://application:,,,/" + "WpfApplication1" + ";component//images/over_cross.png", UriKind.Absolute)), new Rect(0, 0, 50, 50)));
                            }

                            border.Background = new SolidColorBrush(Colors.White);
                            img = new Image
                            {
                                Source = new DrawingImage(group)
                            };
                            border.Child = img;
                        }
                      
                    }));
                });
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (notActivated)
            {
                Window1 gameMenu = new Window1();
                gameMenu.Owner = this;
                gameMenu.ShowDialog();
                notActivated = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (soundPlay)
            {
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri("images/btn/sound-off.png", UriKind.Relative);
                bi3.EndInit();
                myMediaElement.Volume = 0;
                soundPlay = false;
                SoundIcon.Source = bi3;
            }
            else
            {
                BitmapImage bi3 = new BitmapImage();
                bi3.BeginInit();
                bi3.UriSource = new Uri("images/btn/sound-on.png", UriKind.Relative);
                bi3.EndInit();
                myMediaElement.Volume = 100;
                soundPlay = true;
                SoundIcon.Source = bi3;
            }
        }
        Boolean pause = false;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            pause = true;
            Pause w2 = new Pause();
            w2.Owner = this;
            w2.Width = this.ActualWidth;
            w2.Height = this.ActualHeight;
            w2.WindowStyle = WindowStyle.None;
            w2.WindowState = WindowState.Maximized;
            w2.ResizeMode = ResizeMode.NoResize;
            w2.Topmost = true;
            w2.ShowDialog();
            pause = false;
        }
        public void save(string name)
        {
            string saveToFilePath = "save/" + name + ".cross_save";
            StringBuilder line = new StringBuilder();
            foreach (Border b in canvas.Children)
            {
                line.Append(b.Resources["status"]).Append(",");
            }

            using (Stream myStream = File.Open(saveToFilePath, FileMode.Create, FileAccess.ReadWrite))
            {
                StreamWriter myWriter = new StreamWriter(myStream);
                myWriter.WriteLine(currentColor);
                myWriter.WriteLine(currentColorIndex);
                myWriter.WriteLine(levelHigt);
                myWriter.WriteLine(levelName);
                myWriter.WriteLine(seconds);
                myWriter.WriteLine(currentLevelCrossCount);
                myWriter.WriteLine(warningsCount);
                myWriter.WriteLine(allWarningsCount);
                myWriter.WriteLine(line.ToString());
                myStream.Flush();
                myWriter.Flush();
            }
        }
        public async void load(string name)
        {
            string loadToFilePath = "save/" + name + ".cross_save";
            String line = "";
            using (Stream myStream = File.Open(loadToFilePath, FileMode.Open, FileAccess.Read))
            {
                StreamReader myWriter = new StreamReader(myStream);
                currentColor = myWriter.ReadLine();
                currentColorIndex = int.Parse(myWriter.ReadLine());
                levelHigt = int.Parse(myWriter.ReadLine());
                levelName = int.Parse(myWriter.ReadLine());
                seconds = int.Parse(myWriter.ReadLine());
                currentLevelCrossCount = int.Parse(myWriter.ReadLine());
                warningsCount = int.Parse(myWriter.ReadLine());
                allWarningsCount = int.Parse(myWriter.ReadLine());
                Warnings.Text = "Ошибок: " + warningsCount.ToString();
                line = myWriter.ReadLine();
                myWriter.ReadToEnd();
                myStream.Flush();
            }
            canvas.Children.Clear();
            wrap.Children.Clear();
            Window2 w2 = new Window2();
            w2.Owner = this;
            w2.Width = this.ActualWidth;
            w2.Height = this.ActualHeight;
            w2.WindowStyle = WindowStyle.None;
            w2.WindowState = WindowState.Maximized;
            w2.ResizeMode = ResizeMode.NoResize;
            w2.Topmost = true;

            await Task.Run(() =>
            {
                readScheme();
            });

            String[] lines = line.Split(',');
            int i = 0;
            foreach (Border b in canvas.Children)
            {
                try
                {
                    b.Resources["status"] = lines[i++];
                }
                catch (Exception)
                {

                }
            }
            loadedRender();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
        }
    }
}