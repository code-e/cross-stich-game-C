﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    public partial class levels3 : Window
    {
        public levels3()
        {
            InitializeComponent();
        }
        private void select_level_1(object sender, RoutedEventArgs e)
        {
            MainWindow main = ((MainWindow)Owner);

            main.levelName = int.Parse(((TextBlock)((Button)sender).Resources["Level"]).Text);

            main.InitializeComponent2();

            GetWindow(this).Close();


        }
        private void cancel(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            Window1 w1 = new Window1();
            w1.Owner = ((MainWindow)Owner);
            w1.ShowDialog();
        }
    }
}
