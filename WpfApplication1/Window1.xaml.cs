﻿using System.Windows;
using System.Windows.Controls;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private Canvas canvas;
        public Window1()
        {
            InitializeComponent();
        }
        public Window1(Canvas _canvas)
        {
            InitializeComponent();
            canvas = _canvas;
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            Window3 w3 = new Window3();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
        }

        private void button_save(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            ((MainWindow)Owner).save("MyFirstSave");
        }

        private void button_load(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            ((MainWindow)Owner).load("MyFirstSave");
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Opacity = 0;
            Rules w3 = new Rules();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
            GetWindow(this).Opacity = 1;
        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {

            GetWindow(this).Opacity = 0;
            Exit w3 = new Exit();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
            GetWindow(this).Opacity = 1;
        }
        private void button6_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Opacity = 0;
            About w3 = new About();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
            GetWindow(this).Opacity = 1;
        }
    }
}
