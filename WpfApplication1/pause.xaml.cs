﻿using System.Windows;

namespace WpfApplication1
{
    /// <summary>
    /// Логика взаимодействия для Window2.xaml
    /// </summary>
    public partial class Pause : Window
    {
        public Pause()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void button6_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(this).Close();
            ((MainWindow)Owner).save("MyFirstSave");
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            Exit w3 = new Exit();
            w3.Owner = ((MainWindow)Owner);
            w3.ShowDialog();
        }
    }
}
