﻿using System;
using System.Windows.Media;
using System.Windows.Media.Animation;
namespace WpfApplication1
{
    class Sound
    {
        public static void play(String path, Boolean soundPlay)
        {
            if (soundPlay)
            {
                MediaPlayer mp3 = new MediaPlayer();
                mp3.Open(new Uri(path, UriKind.Relative));
                mp3.Play();
            }
        }
    }
}